
// 1 to 4
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())

.then((json) => {
	json.map(todos => console.log(todos.title))
})

// 5 to 6
fetch("https://jsonplaceholder.typicode.com/todos/19")
.then((response) => response.json())
.then((json) => console.log(`Here is the task assigned to you: ${json.title} - completion status: ${json.completed}`))

// 7
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},

	body: JSON.stringify({
		title: "Clean the Kitchen",
		completed: false,
		userId: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json))

// 8
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: true
	})
})

.then((response) => response.json())
.then((json) => console.log(json))

// 9 
fetch("https://jsonplaceholder.typicode.com/todos/2", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Clean the living room",
		description: "Make sure the entire area is clean and organized",
		status: true,
		dateCompleted: "09-05-2022",
		userId: 2

	})
})

.then((response) => response.json())
.then((json) => console.log(json))

// 10
fetch("https://jsonplaceholder.typicode.com/todos/3", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: true
	})
})

.then((response) => response.json())
.then((json) => console.log(json))

// 11
fetch("https://jsonplaceholder.typicode.com/todos/5", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		status: true,
		dateCompleted: "09-09-2022"
	})
})

.then((response) => response.json())
.then((json) => console.log(json))

// 12
fetch("https://jsonplaceholder.typicode.com/todos/15", {
	method: "DELETE"
})
























